from django.apps import AppConfig


class ChangethemeConfig(AppConfig):
    name = 'changetheme'
